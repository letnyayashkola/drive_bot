# Info

Бот для трипшеринга.

# Env

| Переменная | Назначение | Значение по умолчанию | Обязательно |
| ------------- | ------------- | ------- | ----- |
| DRIVE_BOT_TOKEN | Телеграмовский токен | - | да |
| DRIVE_BOT_CHANNEL_ID | Идентификатор канала | - | да |
| DRIVE_BOT_PROXY | Прокси url* | - | нет |
| DRIVE_BOT_URL | url для вебхука | - | нет |
| DRIVE_BOT_PORT | Порт вебхука | 5000 | нет |
| DRIVE_BOT_REDIS_URL | Redis url | redis://localhost | да |

> \* Только socks5. Может быть полезно, при работе из стран, в которых заблокирован Телеграм.

# Dev

Должен стоять docker, docker-compose и poetry

Установка зависимостей:

```bash
$ poetry install
```

Инициализация рабочего окружения

```bash
$ make init_env
```

Команда создаст `.env` файл и запустит внешние сервисы-зависимости.
В файл надо вписать нормальные значения.

Запуск бота:

```bash
$ make run
```

Доп команды:

Выключение окружения

```bash
$ make stop_env
```

Запуск окружения по новой

```bash
$ make start_env
```

Удаление окружения и всех данных

```bash
$ make rm_env
```
