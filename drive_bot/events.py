import logging

from telegram import Bot
from telegram.callbackquery import CallbackQuery
from telegram.ext import CallbackContext
from telegram.message import Message
from telegram.update import Update as StateUpdate

from . import config
from .helpers import check_admin, format_trip
from .job import delete_trip

LOG = logging.getLogger(__name__)


def get_user_data(context: CallbackContext) -> dict:
    return context._dispatcher.user_data


def find_user(context: CallbackContext, msg_id: int):
    storage: dict = get_user_data(context)

    for user_id, data in storage.items():
        for trip_id, trip in data["trip_log"].items():
            if trip["_published_id"] == msg_id:
                return user_id, trip_id

    return None, None


def set_user_state(context: CallbackContext, user_id: int, state: bool):
    data: dict = get_user_data(context)[user_id]
    data["banned"] = state


def on_first_ban(context: CallbackContext, user_id: int, trip_id: int):
    storage: dict = get_user_data(context)

    trip: dict = storage[user_id]["trip_log"].get(trip_id)
    if trip is None:
        return

    trip["status"] = config.STATUS.delete_by_admin
    del storage[user_id]["trip_log"][trip_id]

    del trip["deadline"]

    delete_trip(trip, context)


def ban_unban_event_unauth(update: StateUpdate, _):
    query: CallbackQuery = update.callback_query
    query.answer(text=config.ADMIN_UNATH_TEXT)


@check_admin(ban_unban_event_unauth)
def ban_unban_event(update: StateUpdate, context: CallbackContext):
    query: CallbackQuery = update.callback_query

    _, action, type_, id_ = query.data.split("-")

    if type_ == "f":
        user_id, trip_id = find_user(context, int(id_))
        if user_id is None:
            LOG.error("Cant find user_id by msg_id: %s", id_)
            return
        on_first_ban(context, user_id, trip_id)
    else:
        user_id = int(id_)

    bot: Bot = context.bot

    if action == "unban":
        config.ADMIN_BAN_USER.inline_keyboard[0][0].callback_data = "bu-ban-i-" + str(
            user_id
        )
        reply_markup = config.ADMIN_BAN_USER

        bot.unban_chat_member(config.CHANNEL_ID, user_id)
        set_user_state(context, user_id, False)
    else:
        config.ADMIN_UNBAN_USER.inline_keyboard[0][
            0
        ].callback_data = "bu-unban-i-" + str(user_id)
        reply_markup = config.ADMIN_UNBAN_USER

        set_user_state(context, user_id, True)
        bot.kick_chat_member(config.CHANNEL_ID, user_id)

    query.answer(text=config.QUERY_DONE_ANSWER_TEXT)
    query.edit_message_reply_markup(reply_markup)


def delete_trip_event(update: StateUpdate, context: CallbackContext):
    query: CallbackQuery = update.callback_query
    message: Message = query.message

    trip: dict = context.user_data["trip_log"].get(message.message_id)
    if trip is None:
        LOG.error("Message not found in log: %s", message.message_id)
        return

    trip["status"] = config.STATUS.delete_by_author
    del trip["deadline"]

    del context.user_data["trip_log"][message.message_id]

    bot: Bot = message.bot
    bot.delete_message(config.CHANNEL_ID, trip["_published_id"])

    query.answer(text=config.QUERY_DONE_ANSWER_TEXT)
    query.edit_message_text(text=format_trip(trip), parse_mode="Markdown")


def inline_button_event(update: StateUpdate, context: CallbackContext):
    query: CallbackQuery = update.callback_query

    if query.data == "delete-trip":
        delete_trip_event(update, context)
    elif query.data.startswith("bu"):
        ban_unban_event(update, context)
    else:
        LOG.error("Unknown query.data %s", query.data)
