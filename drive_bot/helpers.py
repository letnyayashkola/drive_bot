import logging
from functools import wraps

from telegram.ext import CallbackContext
from telegram.update import Update as StateUpdate

from redis import Redis

from . import config

LOG = logging.getLogger(__name__)


def check_admin(on_unauth=None):
    def _check_admin(func):
        @wraps(func)
        def wrapped(update: StateUpdate, context: CallbackContext, *args, **kwargs):
            client: Redis = context.bot.redis
            if not client.sismember("admins", update.effective_user.id):
                LOG.info("Unauthorized access denied for %s", update.effective_user.id)
                if on_unauth is not None:
                    on_unauth(update, context)
                return None
            return func(update, context, *args, **kwargs)

        return wrapped

    return _check_admin


def check_ban(func):
    @wraps(func)
    def wrapped(update: StateUpdate, context: CallbackContext, *args, **kwargs):
        if context.user_data.get("banned", False):
            update.message.reply_text(config.YOU_ARE_BANNED)
            return None
        return func(update, context, *args, **kwargs)

    return wrapped


def format_trip(trip: dict, values: dict = None) -> str:
    if values is None:
        values = config.TRIP_TO_SELF_MSG

    res = ""
    for key, value in values.items():
        if key in trip:
            res += "*" + value + "*: " + trip[key] + "\n"

    return res[:-1]
