from copy import copy
from datetime import datetime, timedelta
from enum import IntEnum, auto

from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackContext, JobQueue
from telegram.message import Message
from telegram.update import Update as StateUpdate

from . import config
from .helpers import check_ban, format_trip
from .job import delete_job


class States(IntEnum):
    WAIT_LICENSE = auto()
    WAIT_NEW = auto()
    WAIT_DATE = auto()
    WAIT_TIME = auto()
    WAIT_FROM = auto()
    WAIT_TO = auto()
    WAIT_TR = auto()
    WAIT_COMMENT = auto()
    WAIT_REPEAT = auto()


@check_ban
def ask_license(update: StateUpdate, context: CallbackContext):
    context.user_data["banned"] = context.user_data.get("banned", False)

    if "msg_ids" in context.user_data:
        del context.user_data["msg_ids"]
    if "trip" in context.user_data:
        del context.user_data["trip"]

    if context.user_data.get("trip_log") is None:
        context.user_data["trip_log"] = {}

    if context.user_data.get("license_done", False):
        update.message.reply_markdown(
            config.NEW_TRIP_TEXT, reply_markup=config.NEW_TRIP_KB
        )
        return States.WAIT_NEW

    update.message.reply_markdown(
        config.LICENSE_TEXT, reply_markup=config.APPLY_LICENSE_KB
    )

    return States.WAIT_LICENSE


@check_ban
def wait_license(update: StateUpdate, context: CallbackContext):
    context.user_data["license_done"] = True

    update.message.reply_markdown(config.NEW_TRIP_TEXT, reply_markup=config.NEW_TRIP_KB)

    return States.WAIT_NEW


@check_ban
def wait_new(update: StateUpdate, context: CallbackContext):
    context.user_data["trip"] = {}
    context.user_data["msg_ids"] = set()

    context.user_data["msg_ids"].add(update.message.message_id)

    return ask_date(update, context)


def ask_date(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_text(
        config.ASK_DATE_TEXT, reply_markup=config.ASK_DATE_KB
    )

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_DATE


@check_ban
def wait_date(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    trip_date = datetime.now()
    for index, item in enumerate(config.ASK_DATE_KB.keyboard):
        if update.message.text == item[0]:
            trip_date = trip_date + timedelta(days=index)
            break

    context.user_data["trip"]["_deadline"] = trip_date
    context.user_data["trip"]["time"] = trip_date.strftime(config.DATE_PATTERN)

    return ask_time(update, context)


def ask_time(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_text(
        config.ASK_TIME_TEXT, reply_markup=config.ASK_TIME_KB
    )

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_TIME


@check_ban
def wait_time(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    # parse time
    if update.message.text == config.ASK_TIME_KB.keyboard[0][0]:
        time = "06:00"
    elif update.message.text == config.ASK_TIME_KB.keyboard[1][0]:
        time = "09:00"
    elif update.message.text == config.ASK_TIME_KB.keyboard[-1][0]:
        time = "23:59"
    else:
        time = update.message.text

    # and merge it with date from previous step
    time_deadline = datetime.strptime(time, config.TIME_PATTERN)
    date_deadline: datetime = context.user_data["trip"]["_deadline"]
    deadline = datetime.combine(date_deadline.date(), time_deadline.time())

    # check that date not in past
    if deadline < datetime.now():
        result: Message = update.message.reply_text(config.TIME_IN_PAST_TEXT)
        context.user_data["msg_ids"].add(result.message_id)
        return States.WAIT_TIME

    deadline = deadline + config.DELETE_TIME_DELTA

    # check that it is first trip from user in this datetime
    for _, trip in context.user_data["trip_log"].items():
        if trip["_deadline"] == deadline:
            result = update.message.reply_text(config.TIME_SLOT_ALREADY_USE_TEXT)
            context.user_data["msg_ids"].add(result.message_id)
            return States.WAIT_TIME

    context.user_data["trip"]["_deadline"] = deadline

    context.user_data["trip"]["time"] += ", " + update.message.text

    return ask_from(update, context)


def ask_from(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_markdown(
        config.FROM_TEXT, reply_markup=config.FROM_TO_KB
    )

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_FROM


@check_ban
def wait_from(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    context.user_data["trip"]["from"] = update.message.text

    return ask_to(update, context)


def ask_to(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_markdown(config.TO_TEXT)

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_TO


@check_ban
def wait_to(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    context.user_data["trip"]["to"] = update.message.text

    return ask_transport(update, context)


def ask_transport(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_text(
        config.TRANSPORT_TEXT, reply_markup=config.TRANSPORT_KB
    )

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_TR


@check_ban
def wait_transport(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    context.user_data["trip"]["transport"] = update.message.text

    return ask_comment(update, context)


def ask_comment(update: StateUpdate, context: CallbackContext):
    result: Message = update.message.reply_markdown(
        config.COMMENT_TEXT, reply_markup=config.COMMENT_KB
    )

    context.user_data["msg_ids"].add(result.message_id)

    return States.WAIT_COMMENT


@check_ban
def wait_comment(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)
    context.user_data["trip"]["comment"] = update.message.text

    return validate_username(update, context)


@check_ban
def skip_comment(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)

    return validate_username(update, context)


def validate_username(update: StateUpdate, context: CallbackContext):
    if update.message.from_user.username is None:
        result: Message = update.message.reply_text(
            config.USERNAME_NOT_SET_TEXT, reply_markup=config.REPEAT_KB
        )

        context.user_data["msg_ids"].add(result.message_id)

        return States.WAIT_REPEAT

    return done(update, context)


@check_ban
def wait_repeat(update: StateUpdate, context: CallbackContext):
    context.user_data["msg_ids"].add(update.message.message_id)
    return validate_username(update, context)


def publish(context: CallbackContext, trip: dict) -> int:
    first_name = str(trip["_author"].get("first_name", ""))
    last_name = str(trip["_author"].get("last_name", ""))
    author_id = str(trip["_author"]["username"])

    trip["author"] = f"[{first_name} {last_name}]"

    markup = InlineKeyboardMarkup(
        [[InlineKeyboardButton(config.CONNECT_AUTHOR_TEXT, url=f"t.me/{author_id}")]]
    )

    text = format_trip(trip, config.TRIP_TO_CHAN_MSG)

    msg: Message = context.bot.send_message(
        config.CHANNEL_ID, text=text, reply_markup=markup, parse_mode="Markdown"
    )

    queue: JobQueue = context.job_queue

    queue.run_once(
        delete_job,
        trip["_deadline"],
        context={"msg_id": trip["_self_msg_id"], "user_data": context.user_data},
    )

    return msg.message_id


def done(update: StateUpdate, context: CallbackContext):
    message: Message = update.message
    message.reply_text(config.DONE_TEXT, reply_markup=config.NEW_TRIP_KB)

    trip: dict = copy(context.user_data["trip"])
    trip["status"] = config.STATUS.published

    trip["deadline"] = trip["_deadline"].strftime(config.DATE_TIME_PATTERN)

    result: Message = message.reply_markdown(
        format_trip(trip), reply_markup=config.TRIP_DELETE_KB
    )

    trip["_author"] = update.message.from_user.to_dict()
    trip["_self_msg_id"] = result.message_id
    trip["_chat_id"] = update.message.chat_id

    published_id = publish(context, trip)

    trip["_published_id"] = published_id

    context.user_data["trip_log"][result.message_id] = trip
    del context.user_data["trip"]

    bot: Bot = message.bot
    for id_ in context.user_data["msg_ids"]:
        bot.delete_message(chat_id=message.chat_id, message_id=id_)

    context.user_data["msg_ids"] = set()

    return States.WAIT_NEW
