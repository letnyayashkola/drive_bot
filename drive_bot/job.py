import logging
import time

from telegram import Bot
from telegram.error import BadRequest
from telegram.ext import CallbackContext
from telegram.ext.callbackcontext import CallbackContext as JobCallbackContext

from . import config
from .helpers import format_trip

LOG = logging.getLogger(__name__)


def delete_trip(trip: dict, context: CallbackContext):
    bot: Bot = context.bot

    retries = 0

    while retries < 3:
        try:
            bot.delete_message(config.CHANNEL_ID, trip["_published_id"])
            break
        except BadRequest:
            LOG.error("Err delete trip from chan: %s", trip)
            retries += 1
            time.sleep(1)

    try:
        bot.edit_message_text(
            format_trip(trip),
            chat_id=trip["_chat_id"],
            message_id=trip["_self_msg_id"],
            parse_mode="Markdown",
        )
    except BadRequest:
        LOG.error("Err edit trip: %s", trip)


def delete_job(context: JobCallbackContext):
    msg_id: int = context.job.context["msg_id"]
    trip: dict = context.job.context["user_data"]["trip_log"].get(msg_id)
    if trip is None:
        return

    trip["status"] = config.STATUS.expired
    del context.job.context["user_data"]["trip_log"][msg_id]

    del trip["deadline"]

    delete_trip(trip, context)
