import os
from collections import namedtuple
from datetime import timedelta

from dotenv import load_dotenv

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
)

from . import __version__

try:
    load_dotenv()
except OSError:
    print(".env file not found")

CHANNEL_ID = os.getenv("DRIVE_BOT_CHANNEL_ID")
CHANNEL_URL = None
FAQ_URL = "https://telegra.ph/LSHDrajv-FAQ-08-31"

AUTHORS_TEXT = (
    f"IT-изба, 2019\nВерсия: {__version__}\n"
    + "Source code: https://gitlab.com/letnyayashkola/drive_bot"
)

LICENSE_TEXT = (
    "Несколько моментов, на которые ты должен обратить внимание.\n\n"
    + "1) Бот будет публиковать твои поездки в канале [ЛШ.Драйв]({})."
    + " Чтобы другие пользователи могли связаться с тобой, к сообщением будет"
    + " прикреплена ссылка на твой профиль.\n\n"
    + "2) Мы собираем анонимную статистику использования бота.\n\n"
    + "3) Мы оставляем за собой право заблокировать тебя, если сочтем "
    + "опубликованный тобой анонс неприемлемым\n\n"
    + "Для продолжения нажми кнопку ниже"
)
APPLY_LICENSE_KB = ReplyKeyboardMarkup([["Принимаю"]], resize_keyboard=True)

NEW_TRIP_TEXT = (
    "Теперь ты можешь добавлять поездки. Они будут появляться в канале "
    + "[ЛШ.Драйв]({}) и будут висеть там, пока не пройдет время "
    + "отправления. Если потеряешь ссылку на канал, всегда можешь отправить "
    + "команду /channel\n\n"
    + "Чтобы создать поездку нажми кнопку ниже"
)


def inject_channel_url(channel_url: str):
    global CHANNEL_URL
    global LICENSE_TEXT
    global NEW_TRIP_TEXT
    CHANNEL_URL = channel_url
    LICENSE_TEXT = LICENSE_TEXT.format(channel_url)
    NEW_TRIP_TEXT = NEW_TRIP_TEXT.format(channel_url)


NEW_TRIP_KB = ReplyKeyboardMarkup([["Добавить поездку"]], resize_keyboard=True)

DATE_PATTERN = "%d/%m"
TIME_PATTERN = "%H:%M"
DATE_TIME_PATTERN = DATE_PATTERN + " " + TIME_PATTERN

DELETE_TIME_DELTA = timedelta(minutes=30)

ASK_DATE_TEXT = (
    "Отлично, ты запустил процесс создания поездки. "
    + "В любой момент ты можешь отправить команду /cancel для отмены.\n\n"
    + "Когда собираешься ехать (выбери из списка ниже)?"
)

ASK_DATE_KB = ReplyKeyboardMarkup(
    [["Сегодня"], ["Завтра"], ["Послезавтра"]], resize_keyboard=True
)

ASK_TIME_TEXT = "Примерно в какое время (выбери из списка ниже)?"


ASK_TIME_KB = ReplyKeyboardMarkup(
    [
        ["Ночью (00:00-06:00)"],
        ["Утром (06:00-09:00)"],
        ["09:30"],
        ["10:00"],
        ["10:30"],
        ["11:00"],
        ["11:30"],
        ["12:00"],
        ["12:30"],
        ["13:00"],
        ["13:30"],
        ["14:00"],
        ["14:30"],
        ["15:00"],
        ["15:30"],
        ["16:00"],
        ["16:30"],
        ["17:00"],
        ["17:30"],
        ["18:00"],
        ["18:30"],
        ["19:00"],
        ["19:30"],
        ["20:00"],
        ["20:30"],
        ["Вечером (21:00-23:59)"],
    ],
    resize_keyboard=True,
)

TIME_IN_PAST_TEXT = "Время в прошлом. Давай еще раз"
TIME_SLOT_ALREADY_USE_TEXT = (
    "У тебя уже есть поездка в это время. "
    + "Выбери другое время или удали предыдущую поездку"
)

FROM_TEXT = "Напиши место *отправления* или выбери из списка ниже"
TO_TEXT = "Напиши место *назначения* или выбери из списка ниже"

FROM_TO_KB = ReplyKeyboardMarkup(
    [["ЛШ"], ["Дубна"], ["Кимры (Савелово)"], ["Москва"]], resize_keyboard=True
)

TRANSPORT_TEXT = "Напиши на чем собираешься ехать или выбери из списка ниже"
TRANSPORT_KB = ReplyKeyboardMarkup(
    [["Своя машина"], ["Такси"], ["Электричка"], ["BlaBlaCar"]], resize_keyboard=True
)

COMMENT_TEXT = (
    "*Необязательное поле.* Дополнительная информация. Например точное время, "
    + "альтернативный способ связи или место встречи. "
    + "Если нечего сказать, нажми кнопку ниже"
)

COMMENT_KB = ReplyKeyboardMarkup([["Пропустить"]], resize_keyboard=True)

CANCEL_TEXT = "Ок. Я все забыл"
NOTHING_CANCEL_TEXT = "Нечего отменять"

DONE_TEXT = "Поездка опубликована"

TRIP_DELETE_KB = InlineKeyboardMarkup(
    [[InlineKeyboardButton("Удалить", callback_data="delete-trip")]]
)

REMOVE_KB = ReplyKeyboardRemove()

TRIP_TO_MSG = {
    "time": "Дата и время",
    "from": "Откуда",
    "to": "Куда",
    "transport": "Транспорт",
    "comment": "Комментарий",
}

TRIP_TO_SELF_MSG = {**TRIP_TO_MSG, "status": "Статус", "deadline": "Будет доступно до"}

TRIP_TO_CHAN_MSG = {**TRIP_TO_MSG, "author": "Автор"}

Status = namedtuple(
    "Status", ["published", "delete_by_author", "expired", "delete_by_admin"]
)
STATUS = Status(
    "Опубликовано",
    "Удалено автором",
    "Удалено в связи с истекшим сроком",
    "Удалено модератором",
)

CONNECT_AUTHOR_TEXT = "Связаться с автором"

USERNAME_NOT_SET_TEXT = (
    "Для публикации объявления у тебя должен быть "
    + "@username. Установи его и нажми кнопку ниже"
)

REPEAT_KB = ReplyKeyboardMarkup([["Повторить"]], resize_keyboard=True)

NOT_UNDERSTAND_TEXT = "Не понял"

ADMIN_WRONG_FORWARD_FROM_CHAT_ID_TEXT = "Ошибка. Форвард из неизвестного источника"
ADMIN_UNBAN_USER = InlineKeyboardMarkup([[InlineKeyboardButton("Разблокировать")]])
ADMIN_BAN_USER = InlineKeyboardMarkup([[InlineKeyboardButton("Заблокировать")]])
QUERY_DONE_ANSWER_TEXT = "Готово"
ADMIN_UNATH_TEXT = "Нет прав"

YOU_ARE_BANNED = "Тебя заблокировал модератор"


def kb_to_regex(keyboard: ReplyKeyboardMarkup) -> str:
    res = ""

    for row in keyboard.keyboard:
        for i in row:
            i = i.replace("(", r"\(")
            i = i.replace(")", r"\)")
            res += i + "|"

    return res[:-1]
