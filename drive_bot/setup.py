import logging
import multiprocessing
import os
import sys
from datetime import datetime
from functools import partial

import redis

from telegram.ext import (
    BasePersistence,
    CallbackQueryHandler,
    CommandHandler,
    ConversationHandler,
    Filters,
    JobQueue,
    MessageHandler,
    Updater,
)

from . import __version__
from . import config
from . import states
from .events import inline_button_event
from .handlers import (
    authors,
    cancel,
    error,
    faq,
    general_stub,
    get_channel_url,
    my_id,
    on_forwarded,
)
from .job import delete_job
from .redisp import RedisPersistence

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

LOG = logging.getLogger(__name__)


def load_env():
    token = os.getenv("DRIVE_BOT_TOKEN")
    url = os.getenv("DRIVE_BOT_URL")
    proxy = os.getenv("DRIVE_BOT_PROXY")
    port = int(os.getenv("DRIVE_BOT_PORT", "5000"))
    redis_url = os.getenv("DRIVE_BOT_REDIS_URL", "redis://localhost")

    if not token:
        LOG.error("DRIVE_BOT_TOKEN should be defined")
        sys.exit(1)

    if not config.CHANNEL_ID:
        LOG.error("DRIVE_BOT_CHANNEL_ID should be defined")
        sys.exit(1)

    if url and url.endswith("/"):
        url = url[:-1]

    return token, url, port, redis_url, proxy


def build_conversation():
    conv_handler = ConversationHandler(
        name="main_handler",
        persistent=True,
        allow_reentry=True,
        entry_points=[CommandHandler("start", states.ask_license)],
        states={
            states.States.WAIT_LICENSE: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.APPLY_LICENSE_KB)})$"),
                    states.wait_license,
                )
            ],
            states.States.WAIT_NEW: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.NEW_TRIP_KB)})$"),
                    states.wait_new,
                )
            ],
            states.States.WAIT_DATE: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.ASK_DATE_KB)})$"),
                    states.wait_date,
                )
            ],
            states.States.WAIT_TIME: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.ASK_TIME_KB)})$"),
                    states.wait_time,
                )
            ],
            states.States.WAIT_FROM: [MessageHandler(Filters.text, states.wait_from)],
            states.States.WAIT_TO: [MessageHandler(Filters.text, states.wait_to)],
            states.States.WAIT_TR: [
                MessageHandler(Filters.text, states.wait_transport)
            ],
            states.States.WAIT_COMMENT: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.COMMENT_KB)})$"),
                    states.skip_comment,
                ),
                MessageHandler(Filters.text, states.wait_comment),
            ],
            states.States.WAIT_REPEAT: [
                MessageHandler(
                    Filters.regex(f"^({config.kb_to_regex(config.REPEAT_KB)})$"),
                    states.wait_repeat,
                )
            ],
        },
        fallbacks=[
            CommandHandler("cancel", cancel),
            MessageHandler(Filters.all, general_stub),
        ],
    )

    return conv_handler


def init_dispatcher(updater: Updater):
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CallbackQueryHandler(inline_button_event))
    dispatcher.add_handler(MessageHandler(Filters.forwarded, on_forwarded))
    dispatcher.add_handler(CommandHandler("faq", faq))
    dispatcher.add_handler(CommandHandler("channel", get_channel_url))
    dispatcher.add_handler(CommandHandler("myid", my_id))
    dispatcher.add_handler(CommandHandler("whoisyourdaddy", authors))
    dispatcher.add_handler(build_conversation())
    dispatcher.add_error_handler(error)
    dispatcher.add_handler(MessageHandler(Filters.all, general_stub))


def _restore_jobs(trip: dict, queue: JobQueue, user_data: dict):
    queue.run_once(
        delete_job,
        trip["_deadline"],
        context={
            "msg_id": trip["_self_msg_id"],
            "user_data": user_data[trip["_chat_id"]],
        },
    )


def restore_jobs(store: BasePersistence) -> list:
    users_data = store.get_user_data()

    restore = []
    now = datetime.now()

    for chat_id in users_data:
        for msg_id in users_data[chat_id].get("trip_log", {}):
            if users_data[chat_id]["trip_log"][msg_id]["_deadline"] <= now:
                users_data[chat_id]["trip_log"][msg_id]["_deadline"] = 0

            restore.append(
                partial(_restore_jobs, users_data[chat_id]["trip_log"][msg_id])
            )

    return restore


def request_kwargs(proxy):
    kwargs: dict = {"read_timeout": 10, "connect_timeout": 10}

    if proxy is not None:
        kwargs["proxy_url"] = proxy

    return kwargs


def main():
    token, url, port, redis_url, proxy = load_env()

    client = redis.from_url(redis_url)
    persist_store = RedisPersistence(client)

    restore_funcs = restore_jobs(persist_store)

    updater = Updater(
        token=token,
        request_kwargs=request_kwargs(proxy),
        persistence=persist_store,
        use_context=True,
        workers=multiprocessing.cpu_count(),
    )

    config.inject_channel_url(updater.bot.export_chat_invite_link(config.CHANNEL_ID))

    for func in restore_funcs:
        func(updater.job_queue, updater.dispatcher.user_data)

    updater.bot.redis = client

    init_dispatcher(updater)

    if url:
        updater.start_webhook(listen="0.0.0.0", port=port, url_path=token)
        updater.bot.set_webhook(url=f"{url}/{token}")
    else:
        updater.start_polling()

    LOG.info("Start bot version: %s", __version__)
    LOG.info({k: v for k, v in os.environ.items() if k.startswith("DRIVE_BOT")})

    updater.idle()
