import pickle
from collections import defaultdict

from telegram.ext import BasePersistence


class RedisPersistence(BasePersistence):
    def __init__(self, client, store_user_data=True, store_chat_data=True):
        super().__init__(store_user_data, store_chat_data)

        self._client = client

    def _load_all(self, key, key_parse_func):
        result = defaultdict(dict)

        for key, value in self._client.hgetall(key).items():
            result[key_parse_func(key)] = pickle.loads(value)

        return result

    def get_user_data(self):
        return self._load_all("user_data", int)

    def get_chat_data(self):
        return self._load_all("chat_data", int)

    def get_conversations(self, name):
        key = "conversation_" + str(name)

        return self._load_all(key, pickle.loads)

    def update_conversation(self, name, key, new_state):
        self._client.hset(
            "conversation_" + str(name), pickle.dumps(key), pickle.dumps(new_state)
        )

    def update_user_data(self, user_id, data):
        self._client.hset("user_data", user_id, pickle.dumps(data))

    def update_chat_data(self, chat_id, data):
        self._client.hset("chat_data", chat_id, pickle.dumps(data))

    def flush(self):
        self._client.close()
