import logging

from telegram import Bot
from telegram.ext import CallbackContext, Updater, run_async
from telegram.update import Update as StateUpdate

from . import config
from .helpers import check_admin, check_ban
from .states import States

LOG = logging.getLogger(__name__)


@check_ban
def cancel(update: StateUpdate, context: CallbackContext):
    data: dict = context.user_data
    if not data.get("license_done", False):
        return States.WAIT_LICENSE

    if data.get("trip") is None:
        update.message.reply_text(config.NOTHING_CANCEL_TEXT)
        return States.WAIT_NEW

    del data["trip"]

    bot: Bot = update.message.bot

    for id_ in context.user_data["msg_ids"]:
        bot.delete_message(chat_id=update.message.chat_id, message_id=id_)

    update.message.reply_text(config.CANCEL_TEXT, reply_markup=config.NEW_TRIP_KB)

    context.user_data["msg_ids"] = set()

    return States.WAIT_NEW


def general_stub(update: Updater, context: CallbackContext):
    update.message.reply_text(config.NOT_UNDERSTAND_TEXT)


@run_async
@check_admin(general_stub)
def on_forwarded(update: StateUpdate, _):
    if config.CHANNEL_ID != str(update.message.forward_from_chat.id):
        update.message.reply_text(config.ADMIN_WRONG_FORWARD_FROM_CHAT_ID_TEXT)
        return

    msg_id = update.message.forward_from_message_id

    config.ADMIN_BAN_USER.inline_keyboard[0][0].callback_data = "bu-ban-f-" + str(
        msg_id
    )

    update.message.reply_text(
        update.effective_message.text, reply_markup=config.ADMIN_BAN_USER
    )


@run_async
def get_channel_url(update: Updater, _):
    update.message.reply_text(config.CHANNEL_URL)


@run_async
def authors(update: Updater, _):
    update.message.reply_text(config.AUTHORS_TEXT)


@run_async
def my_id(update: Updater, _):
    update.message.reply_text(update.effective_user.id)


@run_async
def faq(update: Updater, _):
    update.message.reply_text(config.FAQ_URL)


def error(update: Updater, context: CallbackContext):
    """Log Errors caused by Updates."""

    LOG.error('Update "%s" caused error "%s"', update, context.error)
